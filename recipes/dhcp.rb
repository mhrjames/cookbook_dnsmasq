include_recipe 'dnsmasq::default'

execute "Update server ip in /etc/hosts file" do
  command   "sed -ie 's/127.0.1.1/#{node[:dnsmasq][:server_ip]}/' /etc/hosts"
end

execute "Update razornet in /etc/hosts file" do
  command   "sed -ie '/vagrant/a\\#{node[:dnsmasq][:server_ip]}    razor.razornet.local' /etc/hosts"
end

template '/etc/dnsmasq.conf' do
  source 'dynamic_config.erb'
  mode 0644
  variables(
    :config => node[:dnsmasq][:dhcp].to_hash
  )
  notifies :restart, resources(:service => 'dnsmasq'), :immediately
end

