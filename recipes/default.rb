package "dnsmasq" do
  action :upgrade
end


#file node[:dnsmasq][:conf_file] do
#  mode 0644
#  owner "root"
#  group "root"
#  content "conf-dir=#{node[:dnsmasq][:conf_dir]}\n"
#end


if(node[:dnsmasq][:enable_dns])
  #include_recipe 'hosts_file'
end

service 'dnsmasq' do
  action [:enable, :start]
#  if(node[:dnsmasq][:enable_dns])
#    #subscribes :restart, resources(:template => 'managed_hosts_file'), :immediately
#	subscribes :restart, :immediately    
#  end
end

if(node[:dnsmasq][:enable_dns])
  include_recipe 'dnsmasq::dns'
end

if(node[:dnsmasq][:enable_dhcp])
  include_recipe 'dnsmasq::dhcp'
end
