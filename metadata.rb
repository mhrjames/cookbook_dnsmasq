name 'dnsmasq-updated'
maintainer 'James Ahn'
maintainer_email 'chrisroberts.code@gmail.com'
license 'Apache 2.0'
description 'Installs and configures dnsmasq'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '0.2.6'

#depends 'hosts_file'1.0'
#depends "dnsmasq"
